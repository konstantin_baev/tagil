$ ->
  "use strict"

  App =
    init: ->
      @bindCommentsHandler()
      @bindSendAdMessage()
      # @bindVideo()
      @bindPlaceholders()
      return

    bindCommentsHandler: ->
      $('.js-video-comments-handler').on "click", ->
        that = @
        if $(@).hasClass('open')
          $('.c__comments').slideUp "fast", ->
            $(that).removeClass('open')
            $('.c__comments-label').text('Показать')
            false
        else
          $('.c__comments').slideDown "fast", ->
            $('.c__comments-label').text('Скрыть')
            $(that).addClass('open')
            false
        false

      $('.js-video-comments-hide').on "click", ->
        $('.c__comments').slideUp "fast", ->
          $('.js-video-comments-handler').removeClass('open')
          $('.c__comments-label').text('Показать')
          false
        false

      return

    bindSendAdMessage: ->
      $('.js-ad-send-handler').on "click", ->
        $(@).addClass('hide')
        $('.c__ad-thankyou').removeClass('hide')
        false

      $('.c__ad-thankyou').on "click", ->
        $(@).addClass('hide')
        $('.js-ad-send-handler').removeClass('hide')
        false

      return

    bindPlaceholders: ->
      unless Modernizr.input.placeholder
         $('input, textarea').placeholder()
      return

    # bindVideo: ->
    #   $('.js-video-handler').on "click", ->
    #     $(@).find('.c__video-block__image').addClass('hide')
    #     $(@).find('.c__video-block__player').removeClass('hide').click()
    #   return

  App.init()
  return
