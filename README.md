How to use:
---------------
  - install [http://middlemanapp.com](http://middlemanapp.com)
  - open terminal
  - install gems: `bundle install`
  - run middleman: `middleman server`
